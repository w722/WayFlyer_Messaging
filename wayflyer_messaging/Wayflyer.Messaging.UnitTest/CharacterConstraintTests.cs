﻿using Xunit;
using FluentAssertions;
using System;

namespace Wayflyer.Messaging.UnitTest
{
    public class CharacterConstraintTests
    {
        [Fact]
        public void CharacterConstraintValueTest()
        {
            var charInfo = new CharacterConstraintInformation(1, 2, 'a');
            charInfo.Digit1.Should().Be(1);
            charInfo.Digit2.Should().Be(2);
            charInfo.Character.Should().Be('a');
            charInfo.ToString().Should().Be("1-2 a");
        }

        [Fact]
        public void CharacterConstraintToLower()
        {
            var charInfo = new CharacterConstraintInformation(1, 2, 'A');
            charInfo.Digit1.Should().Be(1);
            charInfo.Digit2.Should().Be(2);
            charInfo.Character.Should().Be('a');
            charInfo.ToString().Should().Be("1-2 a");
        }

        [Fact]
        public void CharacterConstraintAllowZero()
        {
            // These are validated in constraint specific way, so are allowed
            var charInfo = new CharacterConstraintInformation(0, 0, '-');
            charInfo.Digit1.Should().Be(0);
            charInfo.Digit2.Should().Be(0);
            charInfo.Character.Should().Be('-');
            charInfo.ToString().Should().Be("0-0 -");
        }

        [Fact]
        public void CharacterConstraintAllowDecreasingOrder()// We allow it but the range constraint will fail
        {
            // These are validated in constraint specific way, so are allowed
            var charInfo = new CharacterConstraintInformation(4, 1, '-');
            charInfo.Digit1.Should().Be(4);
            charInfo.Digit2.Should().Be(1);
            charInfo.Character.Should().Be('-');
            charInfo.ToString().Should().Be("4-1 -");
        }

        [Fact]
        public void CharacterConstraintDoesNotAllowNegative()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => new CharacterConstraintInformation(-1, 0, '-'));
            Assert.Throws<ArgumentOutOfRangeException>(() => new CharacterConstraintInformation(1, -1, '-'));
        }
    }
}
