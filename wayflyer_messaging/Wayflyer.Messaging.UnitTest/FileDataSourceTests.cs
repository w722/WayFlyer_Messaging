﻿using System;
using FluentAssertions;
using Microsoft.Extensions.Logging;
using NSubstitute;
using System.Threading.Tasks;
using Xunit;
using Wayflyer.Messaging.DataSource;
using Wayflyer.Messaging.Serialiser;
using System.Threading;
using System.IO;
using System.Collections.Generic;

namespace Wayflyer.Messaging.UnitTest
{
    public class FileDataSourceTests
    {
        [Fact]
        public async Task CatchesSerialiserErrorAndLogsThem()
        {
            var msgSerialiser = Substitute.For<IMessageSerialiser<WayflyerMessageV1>>();
            var logger = Substitute.For<ILogger>();

            var ex = new ArgumentOutOfRangeException("some error");

            msgSerialiser.Deserialise(Arg.Any<string>()).Returns(Task.FromException<WayflyerMessageV1>(ex));

            var dataSource = new FileDataSource<WayflyerMessageV1>("input2.txt", msgSerialiser, logger);

            int count = 0;

            await foreach (var msg in dataSource.GetMessagesAsync(CancellationToken.None))
            {
                ++count;
            }

            // Check we logged all the errors
            logger.Received(1000).LogError($"Failed to deserialise:input2.txt error: {ex}");

            // All exception
            count.Should().Be(0);
        }

        [Fact]
        public async Task ReadSameFileFromMultipleThreads()
        {
            var msgSerialiser = new FormatStringMessageSerialiser();
            var logger = Substitute.For<ILogger>();

            var dataSource = new FileDataSource<WayflyerMessageV1>("input2.txt", msgSerialiser, logger);

            int count1 = 0;
            int count2 = 0;

            Task thread1 = Task.Run(async () =>
            {
                await foreach (var msg in dataSource.GetMessagesAsync(CancellationToken.None))
                {
                    ++count1;
                }
            });

            Task thread2 = Task.Run(async () =>
            {
                await foreach (var msg in dataSource.GetMessagesAsync(CancellationToken.None))
                {
                    ++count2;
                }
            });

            await Task.WhenAll(thread1, thread2);

            count1.Should().Be(count2);
            count1.Should().Be(1000);
            thread1.Status.Should().Be(TaskStatus.RanToCompletion);
            thread2.Status.Should().Be(TaskStatus.RanToCompletion);
        }

        [Fact]
        public async Task ThrowsWhenFileDoesNotExist()
        {
            var msgSerialiser = Substitute.For<IMessageSerialiser<WayflyerMessageV1>>();
            var logger = Substitute.For<ILogger>();

            var dataSource = new FileDataSource<WayflyerMessageV1>("DoesNotExist", msgSerialiser, logger);

            await Assert.ThrowsAsync<FileNotFoundException>(async () =>
            {
                await foreach (var msg in dataSource.GetMessagesAsync(CancellationToken.None))
                {
                }
            });
        }
    }
}
