﻿using FluentAssertions;
using System;
using System.Threading.Tasks;
using Wayflyer.Messaging.Serialiser;
using Xunit;

namespace Wayflyer.Messaging.UnitTest
{
    public class MessageSerialisationTests
    {
        [Fact]
        public async Task FormatStringMessageSerialiserAndDeserialiseV1()
        {
            IMessageSerialiser<WayflyerMessageV1> s = new FormatStringMessageSerialiser();

            var msg = new WayflyerMessageV1(new CharacterConstraintInformation(1, 2, 'a'), "abcdef");

            var str = await s.Serialise(msg);

            str.Should().Be(msg.ToString());
            str.Should().Be("1-2 a: abcdef");

            var newMsg = await s.Deserialise(str);

            newMsg.CharInfo.Should().BeEquivalentTo(msg.CharInfo);
            newMsg.SearchString.Should().BeEquivalentTo(msg.SearchString);
        }

        [Fact]
        public async Task FormatStringMessageSerialiserAndDeserialiseV1WithUpperCaseSeachString()
        {
            IMessageSerialiser<WayflyerMessageV1> s = new FormatStringMessageSerialiser();

            var msg = new WayflyerMessageV1(new CharacterConstraintInformation(1, 2, 'a'), "ABCDEF");

            msg.SearchString.Should().Be("abcdef");

            var str = await s.Serialise(msg);

            str.Should().Be(msg.ToString());
            str.Should().Be("1-2 a: abcdef");

            var newMsg = await s.Deserialise(str);

            newMsg.CharInfo.Should().BeEquivalentTo(msg.CharInfo);
            newMsg.SearchString.Should().BeEquivalentTo(msg.SearchString);
        }

        [Fact]
        public async Task FormatStringMessageSerialiserAndDeserialiseV1WithUpperCaseCharacter()
        {
            IMessageSerialiser<WayflyerMessageV1> s = new FormatStringMessageSerialiser();

            var msg = new WayflyerMessageV1(new CharacterConstraintInformation(1, 2, 'A'), "abcdef");

            msg.SearchString.Should().Be("abcdef");
            msg.CharInfo.Character.Should().Be('a');

            var str = await s.Serialise(msg);

            str.Should().Be(msg.ToString());
            str.Should().Be("1-2 a: abcdef");

            var newMsg = await s.Deserialise(str);

            newMsg.CharInfo.Should().BeEquivalentTo(msg.CharInfo);
            newMsg.SearchString.Should().BeEquivalentTo(msg.SearchString);
        }

        [Fact]
        public async Task FormatStringMessageSerialiserAndDeserialiseV1DigitsZero()
        {
            IMessageSerialiser<WayflyerMessageV1> s = new FormatStringMessageSerialiser();

            var msg = new WayflyerMessageV1(new CharacterConstraintInformation(0, 0, 'a'), "abcdef");

            var str = await s.Serialise(msg);

            str.Should().Be(msg.ToString());
            str.Should().Be("0-0 a: abcdef");

            var newMsg = await s.Deserialise(str);

            newMsg.CharInfo.Should().BeEquivalentTo(msg.CharInfo);
            newMsg.SearchString.Should().BeEquivalentTo(msg.SearchString);
        }

        [Fact]
        public async Task FormatStringMessageSerialiserAndDeserialiseV1SDigitsSame()
        {
            IMessageSerialiser<WayflyerMessageV1> s = new FormatStringMessageSerialiser();

            var msg = new WayflyerMessageV1(new CharacterConstraintInformation(1, 1, 'a'), "abcdef");

            var str = await s.Serialise(msg);

            str.Should().Be(msg.ToString());
            str.Should().Be("1-1 a: abcdef");

            var newMsg = await s.Deserialise(str);

            newMsg.CharInfo.Should().BeEquivalentTo(msg.CharInfo);
            newMsg.SearchString.Should().BeEquivalentTo(msg.SearchString);
        }

        [Fact]
        public async Task FormatStringMessageSerialiserAndDeserialiseV1DiffLengthCharDigits()
        {
            IMessageSerialiser<WayflyerMessageV1> s = new FormatStringMessageSerialiser();

            var msg = new WayflyerMessageV1(new CharacterConstraintInformation(111, 1112, 'a'), "abcdef");

            var str = await s.Serialise(msg);

            str.Should().Be(msg.ToString());
            str.Should().Be("111-1112 a: abcdef");

            var newMsg = await s.Deserialise(str);

            newMsg.CharInfo.Should().BeEquivalentTo(msg.CharInfo);
            newMsg.SearchString.Should().BeEquivalentTo(msg.SearchString);
        }

        [Fact]
        public async Task FormatStringMessageSerialiserAndDeserialiseV1MaxRangeCharDigits()
        {
            IMessageSerialiser<WayflyerMessageV1> s = new FormatStringMessageSerialiser();

            var msg = new WayflyerMessageV1(new CharacterConstraintInformation(0, short.MaxValue-1, 'a'), "abcdef");

            var str = await s.Serialise(msg);

            str.Should().Be(msg.ToString());
            str.Should().Be($"0-{short.MaxValue-1} a: abcdef");

            var newMsg = await s.Deserialise(str);

            newMsg.CharInfo.Should().BeEquivalentTo(msg.CharInfo);
            newMsg.SearchString.Should().BeEquivalentTo(msg.SearchString);
        }

        [Fact]
        public async Task FormatStringMessageDeserialiseV1FailsWhenDigitsExceedLimits()
        {
            IMessageSerialiser<WayflyerMessageV1> s = new FormatStringMessageSerialiser();
            await Assert.ThrowsAsync<SerialisationException>(async () => await s.Deserialise("1-9999999999999999 a: abcdef"));

            await Assert.ThrowsAsync<SerialisationException>(async () => await s.Deserialise("9999999999999999-4 a: abcdef"));

            await Assert.ThrowsAsync<SerialisationException>(async () => await s.Deserialise("9999999999999999-9999999999999999 a: abcdef"));
        }

        [Fact]
        public void FormatStringMessageFailsWhenSearchStringExceedLimits()
        {
            IMessageSerialiser<WayflyerMessageV1> s = new FormatStringMessageSerialiser();

            string myMassiveString = new string('a', short.MaxValue);

            Assert.Throws<ArgumentOutOfRangeException>( () => new WayflyerMessageV1(new CharacterConstraintInformation(0, 1, 'a'), myMassiveString));
        }

        [Fact]
        public void FormatStringMessageFailsWhenSearchStringNull()
        {
            IMessageSerialiser<WayflyerMessageV1> s = new FormatStringMessageSerialiser();

            string myNullString = null;

            Assert.Throws<ArgumentException>(() => new WayflyerMessageV1(new CharacterConstraintInformation(0, 1, 'a'), myNullString));
        }

        [Fact]
        public async Task FormatStringMessageDeserialiseV1FailsWithNegative()
        {
            IMessageSerialiser<WayflyerMessageV1> s = new FormatStringMessageSerialiser();
            await Assert.ThrowsAsync<SerialisationException>(async () => await s.Deserialise("-1-0 a: abcdef"));

            await Assert.ThrowsAsync<SerialisationException>(async () => await s.Deserialise("1--4 a: abcdef"));

            await Assert.ThrowsAsync<SerialisationException>(async () => await s.Deserialise("-1--4 a: abcdef"));
        }

        [Fact]
        public async Task FormatStringMessageDeserialiseV1FailsWithMissingDigit()
        {
            IMessageSerialiser<WayflyerMessageV1> s = new FormatStringMessageSerialiser();
            await Assert.ThrowsAsync<SerialisationException>(async () => await s.Deserialise("1 a: abcdef"));

            await Assert.ThrowsAsync<SerialisationException>(async () => await s.Deserialise("-2 a: abcdef"));
        }

        [Fact]
        public async Task FormatStringMessageDeserialiseV1FailsWithMissingSpaceAfterDigit()
        {
            IMessageSerialiser<WayflyerMessageV1> s = new FormatStringMessageSerialiser();
            await Assert.ThrowsAsync<SerialisationException>(async () => await s.Deserialise("1-2a: abcdef"));
        }

        [Fact]
        public async Task FormatStringMessageDeserialiseV1FailsWithMissingCharacter()
        {
            IMessageSerialiser<WayflyerMessageV1> s = new FormatStringMessageSerialiser();
            await Assert.ThrowsAsync<SerialisationException>(async () => await s.Deserialise("1-2 : abcdef"));
        }

        [Fact]
        public async Task FormatStringMessageDeserialiseV1FailsWithMissingSeperator()
        {
            IMessageSerialiser<WayflyerMessageV1> s = new FormatStringMessageSerialiser();
            await Assert.ThrowsAsync<SerialisationException>(async () => await s.Deserialise("1 2 a: abcdef"));
        }

        [Fact]
        public async Task FormatStringMessageDeserialiseV1FailsWithMissingColon()
        {
            IMessageSerialiser<WayflyerMessageV1> s = new FormatStringMessageSerialiser();
            await Assert.ThrowsAsync<SerialisationException>(async () => await s.Deserialise("1-2 a abcdef"));
        }

        [Fact]
        public async Task FormatStringMessageDeserialiseV1FailsWithSpaceAfterCharacterBeforeColon()
        {
            IMessageSerialiser<WayflyerMessageV1> s = new FormatStringMessageSerialiser();
            await Assert.ThrowsAsync<SerialisationException>(async () => await s.Deserialise("1-2 a : abcdef"));
        }

        [Fact]
        public async Task FormatStringMessageDeserialiseV1DeserialisesWithExtraSpacesAfterDigits()
        {
            IMessageSerialiser<WayflyerMessageV1> s = new FormatStringMessageSerialiser();
            WayflyerMessageV1 msg = await s.Deserialise(" 1-2  a: abcdef ");
            msg.CharInfo.Digit1.Should().Be(1);
            msg.CharInfo.Digit2.Should().Be(2);
            msg.CharInfo.Character.Should().Be('a');
            msg.SearchString.Should().Be("abcdef ");
        }

        [Fact]
        public async Task FormatStringMessageDeserialiseV1DeserialisesWithExtraSpacesInDigits()
        {
            IMessageSerialiser<WayflyerMessageV1> s = new FormatStringMessageSerialiser();
            WayflyerMessageV1 msg = await s.Deserialise(" 1 - 2 a: abcdef ");
            msg.CharInfo.Digit1.Should().Be(1);
            msg.CharInfo.Digit2.Should().Be(2);
            msg.CharInfo.Character.Should().Be('a');
            msg.SearchString.Should().Be("abcdef ");
        }

        [Fact]
        public async Task FormatStringMessageDeserialiseV1DeserialisesWithExtraSpacesInSearchString()
        {
            IMessageSerialiser<WayflyerMessageV1> s = new FormatStringMessageSerialiser();
            WayflyerMessageV1 msg = await s.Deserialise(" 1 - 2 a: ab c d ef ");
            msg.CharInfo.Digit1.Should().Be(1);
            msg.CharInfo.Digit2.Should().Be(2);
            msg.CharInfo.Character.Should().Be('a');
            msg.SearchString.Should().Be("ab c d ef ");
        }

        [Fact]
        public async Task FormatStringMessageDeserialiseV1DeserialisesWithExtraSpacesBeforeCharacter()
        {
            IMessageSerialiser<WayflyerMessageV1> s = new FormatStringMessageSerialiser();
            WayflyerMessageV1 msg = await s.Deserialise(" 1-2     a: ab c d ef ");
            msg.CharInfo.Digit1.Should().Be(1);
            msg.CharInfo.Digit2.Should().Be(2);
            msg.CharInfo.Character.Should().Be('a');
            msg.SearchString.Should().Be("ab c d ef ");
        }

        [Fact]
        public async Task FormatStringMessageDeserialiseV1DeserialisesWithEmptyString()
        {
            IMessageSerialiser<WayflyerMessageV1> s = new FormatStringMessageSerialiser();
            WayflyerMessageV1 msg = await s.Deserialise("1-2 a:");// No space after ':'
            msg.SearchString.Should().Be(string.Empty);

            msg = await s.Deserialise("1-2 a: ");// Space
            msg.CharInfo.Digit1.Should().Be(1);
            msg.CharInfo.Digit2.Should().Be(2);
            msg.CharInfo.Character.Should().Be('a');
            msg.SearchString.Should().Be(string.Empty);
        }

        [Fact]
        public async Task FormatStringMessageDeserialiseV1DeserialisesWithDashAsCharacter()
        {
            IMessageSerialiser<WayflyerMessageV1> s = new FormatStringMessageSerialiser();
            WayflyerMessageV1 msg = await s.Deserialise("1-2 -: abcdef");
            msg.CharInfo.Digit1.Should().Be(1);
            msg.CharInfo.Digit2.Should().Be(2);
            msg.CharInfo.Character.Should().Be('-');
            msg.SearchString.Should().Be("abcdef");
        }

        [Fact]
        public async Task FormatStringMessageDeserialiseV1DeserialisesWithColonAsCharacter()
        {
            IMessageSerialiser<WayflyerMessageV1> s = new FormatStringMessageSerialiser();
            WayflyerMessageV1 msg = await s.Deserialise("1-2 :: abcdef");
            msg.CharInfo.Digit1.Should().Be(1);
            msg.CharInfo.Digit2.Should().Be(2);
            msg.CharInfo.Character.Should().Be(':');
            msg.SearchString.Should().Be("abcdef");
        }

        [Fact]
        public async Task FormatStringMessageDeserialiseV1DeserialisesWithSpaceAsCharacter()
        {
            IMessageSerialiser<WayflyerMessageV1> s = new FormatStringMessageSerialiser();
            WayflyerMessageV1 msg = await s.Deserialise("1-2  : abcdef");
            msg.CharInfo.Digit1.Should().Be(1);
            msg.CharInfo.Digit2.Should().Be(2);
            msg.CharInfo.Character.Should().Be(' ');
            msg.SearchString.Should().Be("abcdef");
        }
    }
}
