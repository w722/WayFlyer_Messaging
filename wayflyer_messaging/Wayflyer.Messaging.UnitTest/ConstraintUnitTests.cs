﻿using FluentAssertions;
using Microsoft.Extensions.Logging;
using NSubstitute;
using System.Collections.Generic;
using System.Threading.Tasks;

using Wayflyer.Messaging.Constraint;
using Wayflyer.Messaging.DataSource;
using Wayflyer.Messaging.Serialiser;
using Xunit;

namespace Wayflyer.Messaging.UnitTest
{
    public class MessageConstraintTests
    {
        public static IEnumerable<object[]> MessageDataV1FileInputData()
        {
            yield return new object[] { new string("input2.txt") };
        }

        public static IEnumerable<object[]> MessageDataV1ValidRange()
        {
            yield return new object[] { new WayflyerMessageV1(new CharacterConstraintInformation(0, 0, 'a'), "") };// 0 occurrences of 'a' (valid)
            yield return new object[] { new WayflyerMessageV1(new CharacterConstraintInformation(0, 0, 'a'), "b") };// 0 occurrences of 'a' (valid)
            yield return new object[] { new WayflyerMessageV1(new CharacterConstraintInformation(0, 10, 'a'), "") };// 0 occurrences of 'a'  (valid)
            yield return new object[] { new WayflyerMessageV1(new CharacterConstraintInformation(0, 10, 'a'), "a") };// 1 occurrences of 'a'  (valid)
            yield return new object[] { new WayflyerMessageV1(new CharacterConstraintInformation(1, 1, 'a'), "a") };// 1 occurrences of 'a' (valid)
            yield return new object[] { new WayflyerMessageV1(new CharacterConstraintInformation(1, 10, 'a'), "a") };// 1 occurrences of 'a' (valid)
            yield return new object[] { new WayflyerMessageV1(new CharacterConstraintInformation(1, 10, 'a'), "aaaaaaaaaa") };// 10 (upper bound) occurrences of 'a' (valid)
            yield return new object[] { new WayflyerMessageV1(new CharacterConstraintInformation(1, 10, ' '), "aaaaa aaaaa") };// 1 space (valid)
            yield return new object[] { new WayflyerMessageV1(new CharacterConstraintInformation(1, 10, ' '), " aaaaa-aaaaa") };// 1 space at start (valid)
            yield return new object[] { new WayflyerMessageV1(new CharacterConstraintInformation(1, 2, ' '), " aaaaa-aaaaa ") };// 2 spaces at start and end (valid)
            yield return new object[] { new WayflyerMessageV1(new CharacterConstraintInformation(1, 10, ':'), "aaaaa:aaaaa") };// 1 colon (valid)
            yield return new object[] { new WayflyerMessageV1(new CharacterConstraintInformation(1, 10, ':'), ":aaaaaaaaaa") };// 1 colon at start (valid)
            yield return new object[] { new WayflyerMessageV1(new CharacterConstraintInformation(1, 10, '-'), "aaaaa-aaaaa") };// 1 dash (valid)
            yield return new object[] { new WayflyerMessageV1(new CharacterConstraintInformation(1, 10, '&'), "&aaaa-aaaaa") };// 1 & (valid)
            yield return new object[] { new WayflyerMessageV1(new CharacterConstraintInformation(1, 10, 'A'), "aaaaa-aaaaa") };// 1 a (valid, we force lower)
            yield return new object[] { new WayflyerMessageV1(new CharacterConstraintInformation(1, 10, 'a'), "A") };// 1 a (valid, we force lower)
            yield return new object[] { new WayflyerMessageV1(new CharacterConstraintInformation(1, 2, '2'), "2") };// 1 '2' (valid)
        }

        public static IEnumerable<object[]> MessageDataV1Invalid()
        {
            yield return new object[] { new WayflyerMessageV1(new CharacterConstraintInformation(0, 0, 'a'), "a") };// 1 occurrences of 'a' (invalid)
            yield return new object[] { new WayflyerMessageV1(new CharacterConstraintInformation(1, 0, 'a'), "a") };// bad range (invalid)
            yield return new object[] { new WayflyerMessageV1(new CharacterConstraintInformation(2, 3, 'a'), "a") };// 1 (below lower) occurrences of 'a' (invalid)
            yield return new object[] { new WayflyerMessageV1(new CharacterConstraintInformation(2, 3, 'a'), "aaaa") };// 4 (above upper) occurrences of 'a' (invalid)
            yield return new object[] { new WayflyerMessageV1(new CharacterConstraintInformation(2, 3, 'b'), "aaaa") };// 0 occurrences of 'b' (invalid)
            yield return new object[] { new WayflyerMessageV1(new CharacterConstraintInformation(1, 2, ' '), "aaaa") };// 0 occurrences of ' ' (invalid)
            yield return new object[] { new WayflyerMessageV1(new CharacterConstraintInformation(10, 20, '1'), "1") };// 0 occurrences of ' ' (invalid)
        }

        [Theory]
        [MemberData(nameof(MessageDataV1ValidRange))]
        public async Task ValidRangeContraintChecks(WayflyerMessageV1 msg)
        {
            var rangeConstraint = new RangeCountConstraint();
            var res = await rangeConstraint.IsValid(msg);
            res.Should().BeTrue();
        }

        [Theory]
        [MemberData(nameof(MessageDataV1Invalid))]
        public async Task InvalidRangeContraintChecks(WayflyerMessageV1 msg)
        {
            var rangeConstraint = new RangeCountConstraint();
            var res = await rangeConstraint.IsValid(msg);
            res.Should().BeFalse();
        }

        public static IEnumerable<object[]> MessageDataV1ValidPosition()
        {
            yield return new object[] { new WayflyerMessageV1(new CharacterConstraintInformation(1, 2, 'a'), "a") };// a is at position 1 (valid) even although pos 2 is out of range
            yield return new object[] { new WayflyerMessageV1(new CharacterConstraintInformation(1, short.MaxValue-1, 'a'), "a") };// a is at position 1 (valid) even although pos 2 is out of range
            yield return new object[] { new WayflyerMessageV1(new CharacterConstraintInformation(1, 2, 'a'), "ba") };// a is at position 2 (valid)
            yield return new object[] { new WayflyerMessageV1(new CharacterConstraintInformation(2, 4, ' '), "b abbbbb") };// ' ' is at position 2 (valid)
            yield return new object[] { new WayflyerMessageV1(new CharacterConstraintInformation(1, 2, 'a'), "ba") };// a is at position 2 (valid)
            yield return new object[] { new WayflyerMessageV1(new CharacterConstraintInformation(5, 2, 'a'), "ba") };// a is at position 2 (valid)
        }

        public static IEnumerable<object[]> MessageDataV1InvalidPosition()
        {
            yield return new object[] { new WayflyerMessageV1(new CharacterConstraintInformation(1, 1, 'a'), "a") };// Invalid can be only one
            yield return new object[] { new WayflyerMessageV1(new CharacterConstraintInformation(0, 0, 'a'), "a") };// 0 invalid position
            yield return new object[] { new WayflyerMessageV1(new CharacterConstraintInformation(0, 1, 'a'), "a") };// 0 invalid position
            yield return new object[] { new WayflyerMessageV1(new CharacterConstraintInformation(2, 3, 'a'), "baa") };// Both valid so invalid
            yield return new object[] { new WayflyerMessageV1(new CharacterConstraintInformation(2, 3, 'a'), "") };// Empty string so invalid
        }

        [Theory]
        [MemberData(nameof(MessageDataV1ValidPosition))]
        public async Task ValidPositionContraintChecks(WayflyerMessageV1 msg)
        {
            var posConstraint = new PositionConstraint();
            var res = await posConstraint.IsValid(msg);
            res.Should().BeTrue();
        }

        [Theory]
        [MemberData(nameof(MessageDataV1InvalidPosition))]
        public async Task InvalidPositionContraintChecks(WayflyerMessageV1 msg)
        {
            var posConstraint = new PositionConstraint();
            var res = await posConstraint.IsValid(msg);
            res.Should().BeFalse();
        }

        [Theory]
        [MemberData(nameof(MessageDataV1FileInputData))]
        public async Task RangeConstraintFileTests(string filename)
        {
            var logger = Substitute.For<ILogger>();
            var messageSeriliser = new FormatStringMessageSerialiser();

            var inputSource = new FileDataSource<WayflyerMessageV1>(filename, messageSeriliser, logger);

            // Range contraint
            IMessageConstraint constraint = new RangeCountConstraint();

            // Check how many messages satisfy the range count constraint
            int countConstraint = await ConstraintValidator.GetConstraintCount(inputSource, constraint, System.Threading.CancellationToken.None);

            countConstraint.Should().Be(666);
        }

        [Theory]
        [MemberData(nameof(MessageDataV1FileInputData))]
        public async Task PositionConstraintFileTests(string filename)
        {
            var logger = Substitute.For<ILogger>();
            var messageSeriliser = new FormatStringMessageSerialiser();

            var inputSource = new FileDataSource<WayflyerMessageV1>(filename, messageSeriliser, logger);

            // Range contraint
            IMessageConstraint constraint = new PositionConstraint();

            // Check how many messages satisfy the range count constraint
            int countConstraint = await ConstraintValidator.GetConstraintCount(inputSource, constraint, System.Threading.CancellationToken.None);

            countConstraint.Should().Be(670);
        }

        // Check message 'Satisfies' calls 'IsValid' on constraint (double dispatch - visitor pattern)
        [Fact]
        public async Task CheckMessageVisitorIsCalled()
        {
            var msgConstraint = Substitute.For<IMessageConstraint>();
            IConstrainableMessage msg = new WayflyerMessageV1(new CharacterConstraintInformation(1, 2, 'a'), "a");

            msgConstraint.IsValid(msg as WayflyerMessageV1).Returns(Task.FromResult(true));

            var res = await msg.Satisfies(msgConstraint);

            await msgConstraint.Received(1).IsValid(msg as WayflyerMessageV1);

            res.Should().BeTrue();
        }
    }
}
