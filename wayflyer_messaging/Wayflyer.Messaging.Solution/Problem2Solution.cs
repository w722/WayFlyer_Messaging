﻿using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;
using Wayflyer.Messaging.Constraint;
using Wayflyer.Messaging.DataSource;
using Wayflyer.Messaging.Serialiser;

namespace Wayflyer.Messaging
{
    internal class Problem2Solution
    {
        static async Task Main(string[] args)// TODO: use args to pick data source and specify constraints
        {
            var p = new Problem2Solution();

            // Create a logger (console but could easily add file and/or remote logging)
            using ILoggerFactory loggerFactory =
            LoggerFactory.Create(builder =>
                builder.AddSimpleConsole(options =>
                {
                    options.IncludeScopes = true;
                    options.SingleLine = true;
                    options.TimestampFormat = "hh:mm:ss ";
                }));

            ILogger<Problem2Solution> logger = loggerFactory.CreateLogger<Problem2Solution>();

            // Create our data source for the input file, specifying message version and inject in the correct serialiser
            var messageSeriliser = new FormatStringMessageSerialiser();

            var inputSource = new FileDataSource<WayflyerMessageV1>("input2.txt", messageSeriliser, logger);

            // Firstly we want to check using the count constraint
            IMessageConstraint constraint = new RangeCountConstraint();

            var tokenSource = new CancellationTokenSource();// We may want to cancel the operation in a controlled way (currently unused)

            // tokenSource.Cancel() - This can be used to cancel a long running constraint check

            // Check how many messages satisfy the range count constraint
            int countConstraint = await ConstraintValidator.GetConstraintCount(inputSource, constraint, tokenSource.Token);

            // Now we want to check all the message against our position constraint
            constraint = new PositionConstraint();

            int countPosition = await ConstraintValidator.GetConstraintCount(inputSource, constraint, tokenSource.Token);

            logger.LogInformation($"The number of constraints satisfied for problem 2.a (Range count constraint) is {countConstraint}");
            logger.LogInformation($"The number of constraints satisfied for problem 2.b (Position constraint) is {countPosition}");

            Console.ReadLine();
        }
    }
}
