﻿using System.Threading.Tasks;

namespace Wayflyer.Messaging.Constraint
{
    /// <summary>
    /// Represents a constraint that can be applied to a message
    /// </summary>
    public interface IMessageConstraint
    {
        /// <summary>
        /// Checks whether this V1 message is valid i.e. satifies the constraint
        /// </summary>
        /// <param name="msg">The message to check</param>
        /// <returns>True if contrainst is satisfied</returns>
        Task<bool> IsValid(WayflyerMessageV1 msg);

        // Can be extended for other versions (if required)
        // e.g. Task<bool> IsValid(WayflyerMessageV2 msg);
    }
}
