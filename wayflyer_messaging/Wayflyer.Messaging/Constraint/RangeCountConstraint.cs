﻿using System;
using System.Threading.Tasks;

namespace Wayflyer.Messaging.Constraint
{
    /// <summary>
    /// A message constraint implementation to check whether a character exists within the string a given number of times (within a range).
    /// The number of times must be within a specified range (lowest -> highest). If 'highest' == 'lowest' we just check for that number of occurrences.
    /// </summary>
    public class RangeCountConstraint : IMessageConstraint
    {
        /// <summary>
        /// Check if the range is valid. It is only deemed valid if 'lowest' and 'highest' are > than 0 and 'highest' >= 'lowest'
        /// </summary>
        /// <param name="lowest">The lowest count</param>
        /// <param name="highest">The highest count</param>
        /// <returns>True if range is valid otherwise false</returns>
        private bool IsValidRange(int lowest, int highest)
        {
            // Only valid if from 0 to N where is N is a positive number >= 0
            if (lowest < 0)
            {
                return false;
            }

            if (highest < 0)
            {
                return false;
            }

            if (highest < lowest)// We allow the same but not 'highest' < 'lowest'
            {
                return false;
            }

            // If 'highest' == 'lowest' we just check for that number

            return true;
        }

        /// <summary>
        /// Count the number of times the character occurs in the search string
        /// </summary>
        /// <param name="searchString">The search string to search</param>
        /// <param name="ch">The character to search for</param>
        /// <returns>The instance count of the character in the string</returns>
        private int CountCharacterInstances(string searchString, char ch)
        {
            int charCount = 0;

            foreach (var character in searchString)
            {
                if (character == ch)
                {
                    ++charCount;
                }
            }

            return charCount;
        }

        /// <summary>
        /// Checks whether the V1 message is valid
        /// </summary>
        /// <param name="msg">The V1 message to validate</param>
        /// <returns>True if valid otherwise false</returns>
        public Task<bool> IsValid(WayflyerMessageV1 msg)
        {
            return IsValid(msg.CharInfo, msg.SearchString);
        }

        /// <summary>
        /// Checks whether the specifed character constraint information is valid
        /// </summary>
        /// <param name="charInfo">The character constraint information</param>
        /// <param name="searchString">The search string to search</param>
        /// <returns>True if valid otherwise false</returns>
        private Task<bool> IsValid(CharacterConstraintInformation charInfo, string searchString)
        {
            if (searchString == null)// We allow empty search string but not 'null'
            {
                return Task.FromResult(false);
            }

            // Check we have a valid range
            if (IsValidRange(charInfo.Digit1, charInfo.Digit2))
            {
                // The range is valid for this constraint but we must now check if the constraint itself is valid

                var count = CountCharacterInstances(searchString, charInfo.Character);// Count how may exist

                // If the count is within the specified range then it is valid
                if (count >= charInfo.Digit1 && count <= charInfo.Digit2)
                {
                    return Task.FromResult(true);
                }
            }

            return Task.FromResult(false);
        }
    }
}
