﻿using System.Threading;
using System.Threading.Tasks;
using Wayflyer.Messaging.DataSource;

namespace Wayflyer.Messaging.Constraint
{
    public static class ConstraintValidator
    {
        /// <summary>
        /// Read the data asychonously (piecewise) from the specified data source and check how many messages meet the specified constraint
        /// </summary>
        /// <param name="inputDataSource">The input data source providing the messages</param>
        /// <param name="constraint">The constraint to check for</param>
        /// <param name="token">We may want to cancel the operation in a controlled way</param>
        /// <returns>The number of messages satisfying the constraint</returns>
        public static async Task<int> GetConstraintCount(IDataSource inputDataSource, IMessageConstraint constraint, CancellationToken token)
        {
            int constraintCount = 0;

            await foreach (var msg in inputDataSource.GetMessagesAsync(token))
            {
                var result = await msg.Satisfies(constraint);
                if (result)
                {
                    ++constraintCount;
                }
            }

            return constraintCount;
        }
    }
}
