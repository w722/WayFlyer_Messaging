﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Wayflyer.Messaging.Constraint
{
    /// <summary>
    /// A message constraint implementation to check whether a character exists in a particular position (in exactly one of two possible positions).
    /// If 'first' == 'second' this is invalid.
    /// </summary>
    public class PositionConstraint : IMessageConstraint
    {
        /// <summary>
        /// Check if the positions are valid.
        /// </summary>
        /// <param name="first">The first position</param>
        /// <param name="second">The second position</param>
        /// <returns>True if positions are valid otherwise false</returns>
        private bool IsValidPositions(int first, int second)
        {
            // Only valid if from 1 to N where is N is a positive number > 0
            if (first <= 0)
            {
                return false;
            }

            if (second <= 0)
            {
                return false;
            }

            if (first == second)// There can be only one
            {
                return false;
            }

            // We don't care if position 1 is > than position 2 as long as they are different

            return true;
        }

        /// <summary>
        /// Get the positions that the character exists in the search string
        /// </summary>
        /// <param name="searchString">The search string to search</param>
        /// <param name="ch">The character to search for</param>
        /// <returns>The positions of the character in order or empty list of no occurrences. 1 based positions are returned</returns>
        private List<int> GetCharacterPositions(string searchString, char ch)
        {
            var positions = new List<int>();

            for (int i=0; i < searchString.Length; i++)
            {
                if (searchString[i] == ch)
                {
                    positions.Add(i+1);// 1 based positions
                }
            }

            return positions;
        }

        /// <summary>
        /// Checks whether the V1 message is valid
        /// </summary>
        /// <param name="msg">The V1 message to validate</param>
        /// <returns>True if valid otherwise false</returns>
        public Task<bool> IsValid(WayflyerMessageV1 msg)
        {
            return IsValid(msg.CharInfo, msg.SearchString);
        }

        /// <summary>
        /// Checks whether the specifed character constraint information is valid
        /// </summary>
        /// <param name="charInfo">The character constraint information</param>
        /// <param name="searchString">The search string to search</param>
        /// <returns>True if valid otherwise false</returns>
        private Task<bool> IsValid(CharacterConstraintInformation charInfo, string searchString)
        {
            if (searchString == null)// We allow empty search string but not 'null'
            {
                return Task.FromResult(false);
            }

            // Check we have a valid positions
            if (IsValidPositions(charInfo.Digit1, charInfo.Digit2))
            {
                // The positions are valid for this constraint but we must now check if the constraint itself is valid
                var positions = GetCharacterPositions(searchString, charInfo.Character);
                if (positions.Count == 0)
                {
                    // Not found so cannot be valid
                    return Task.FromResult(false);
                }

                bool position1Valid = false;
                bool position2Valid = false;

                // If one of the positions specified matches any that we found, we are valid.
                foreach (var position in positions)
                {
                    if (position == charInfo.Digit1)
                    {
                        position1Valid = true;
                    }

                    if (position == charInfo.Digit2)
                    {
                        position2Valid = true;
                    }
                }

                if (position1Valid || position2Valid)
                {
                    if (position1Valid && position2Valid)
                    {
                        // Only one allowed - not valid
                        return Task.FromResult(false);
                    }

                    // Only one valid -  we are good
                    return Task.FromResult(true);
                }
            }

            return Task.FromResult(false);
        }
    }
}
