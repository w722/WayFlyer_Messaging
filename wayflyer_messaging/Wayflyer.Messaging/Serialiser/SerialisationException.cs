﻿using System;
using System.Runtime.Serialization;

namespace Wayflyer.Messaging.Serialiser
{
    /// <summary>
    /// Exception that is thrown if we fail to (de)serialise a wayflyer message
    /// </summary>
    [Serializable]
    public class SerialisationException : Exception
    {
        public SerialisationException()
        {
        }

        public SerialisationException(string message) : base(message)
        {
        }

        public SerialisationException(string message, Exception inner) : base(
            message, inner)
        {
        }

        protected SerialisationException(
            SerializationInfo info,
            StreamingContext context) : base(info, context)
        {
        }
    }
}
