﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Wayflyer.Messaging.Serialiser
{
    /// <summary>
    /// This is the format string serialiser used to (de)serialise the wayfler format string message format e.g. n-n c: str 
    /// </summary>
    public class FormatStringMessageSerialiser : IMessageSerialiser<WayflyerMessageV1>
    {
        /// <summary>
        /// 'WayflyerMessageV1' deserialiser
        /// </summary>
        /// <param name="input">The input string</param>
        /// <returns>The V1 message</returns>
        /// <exception cref="SerialisationException"/>
        Task<WayflyerMessageV1> IMessageSerialiser<WayflyerMessageV1>.Deserialise(string input)
        {
            try
            {
                string trimmed = input.ToLower().TrimStart();// Put to lower and remove any leading whitespace

                // Find last ':'
                var searchStringIndex = trimmed.LastIndexOf(':');
                if (searchStringIndex < 0)
                {
                    throw new SerialisationException($"Failed to deserialise {input}");
                }

                // Get character constraint substring (everything before the ':')
                var contraints = trimmed.Substring(0, searchStringIndex);

                // Everything after the ':' is search string
                var searchString = trimmed.Substring(searchStringIndex + 1);
                if (searchString.Length > 0)
                {
                    searchString = searchString.Substring(1);// Remove whitespace
                }

                // The character is immediately prior to the ':'
                var character = contraints[contraints.Length - 1];

                // Get the digit string and check we have at least one space
                var digitsStr = contraints.Substring(0, searchStringIndex - 1);
                if (digitsStr[digitsStr.Length-1] != ' ')// Should be at least 1 space
                {
                    throw new SerialisationException($"Failed to deserialise {input}");
                }

                var parts = digitsStr.Split(' ');// Split and remove any whitespace

                var validated = new List<string>();
                string digitString = "";
                foreach (var item in parts)// Trim the sections (cope with extra spaces)
                {
                    var ts = item.Trim();
                    if (ts != string.Empty)
                    {
                        validated.Add(ts);
                        digitString += ts;// Build digit string
                    }
                }

                if (digitString.Length >= 3)
                {
                    var digits = digitString.Split('-');
                    if (digits.Length == 2)// x2 Numbers
                    {
                        var digitA = short.Parse(digits[0]);// This will throw if larger (we pre-filter negative values)
                        var digitB = short.Parse(digits[1]);

                        var constraintInfo = new CharacterConstraintInformation(digitA, digitB, character);

                        return Task.FromResult(new WayflyerMessageV1(constraintInfo, searchString));
                    }
                }

                throw new SerialisationException($"Failed to deserialise {input}");
            }
            catch (Exception ex)
            {
                throw new SerialisationException($"Failed to deserialise {input}", ex);
            }
        }

        /// <summary>
        /// Serialise a V1 message to a string
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        Task<string> IMessageSerialiser<WayflyerMessageV1>.Serialise(WayflyerMessageV1 msg)
        {
            return Task.FromResult(msg.ToString());
        }
    }
}
