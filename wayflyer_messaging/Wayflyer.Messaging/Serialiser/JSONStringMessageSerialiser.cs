﻿using System;
using System.Text.Json;
using System.Threading.Tasks;

namespace Wayflyer.Messaging.Serialiser
{
    /// <summary>
    ///  A JSON serialiser (Ficticious). We may want to change the way we (de)serialise our messages.
    /// </summary>
    public class JSONStringMessageSerialiser : IMessageSerialiser<WayflyerMessageV1>
    {
        /// <summary>
        /// Deserialise WayflyerMessageV1 from string
        /// </summary>
        /// <param name="input">The JSON string to deserialise</param>
        /// <returns>The V1 Message</returns>
        /// <exception cref="SerialisationException"></exception>
        Task<WayflyerMessageV1> IMessageSerialiser<WayflyerMessageV1>.Deserialise(string input)
        {
            try
            {
                return Task.FromResult((WayflyerMessageV1)JsonSerializer.Deserialize(input, typeof(WayflyerMessageV1)));
            }
            catch (Exception ex)
            {
                throw new SerialisationException($"Failed to deserialise {input}", ex);
            }
        }

        /// <summary>
        /// Serialise WayflyerMessageV1 to string
        /// </summary>
        /// <param name="msg">The V1 message to serialise</param>
        /// <returns>The serialsied string</returns>
        /// <exception cref="SerialisationException"></exception>
        Task<string> IMessageSerialiser<WayflyerMessageV1>.Serialise(WayflyerMessageV1 msg)
        {
            try
            {
                return Task.FromResult(JsonSerializer.Serialize(msg));
            }
            catch (Exception ex)
            {
                throw new SerialisationException($"Failed to serialise {msg}", ex);
            }
        }
    }
}
