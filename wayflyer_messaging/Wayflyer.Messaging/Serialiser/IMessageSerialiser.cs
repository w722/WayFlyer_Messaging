﻿using System.Threading.Tasks;

namespace Wayflyer.Messaging.Serialiser
{
    /// <summary>
    /// A message (de)serialiser for 'IConstrainableMessage' type messages
    /// <seealso cref="FormatStringMessageSerialiser"/><seealso cref="JSONStringMessageSerialiser"/>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IMessageSerialiser<T> where T : IConstrainableMessage // Generic constraint that means all types must be 'IConstrainableMessage'
    {
        /// <summary>
        /// Deserialise the specified string into the specified concrete type 'T'
        /// </summary>
        /// <param name="input">The input string</param>
        /// <returns>The deserialised 'T' object</returns>
        /// <exception cref=""
        public Task<T> Deserialise(string input);

        /// <summary>
        /// Serialise the specified message of type 'T' into a string
        /// </summary>
        /// <param name="msg">The message to serialise</param>
        /// <returns>The serialised string</returns>
        public Task<string> Serialise(T msg);
    }
}
