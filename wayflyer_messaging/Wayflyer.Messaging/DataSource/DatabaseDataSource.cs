﻿using Dapper;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Runtime.CompilerServices;
using System.Threading;

namespace Wayflyer.Messaging.DataSource
{
    /// <summary>
    /// Example of using mirco ORM 'Dapper' to asynchronously use a sql data reader to pull (potentially huge volumes of messages) from a SQL database
    /// EF (Entity Framework) could have also have been used. Messages are processed a row at a time to avoid memory pressures.
    /// USAGE: DatabaseDataSource<WayflyerMessageV1> dbSource;
    /// </summary>
    /// <typeparam name="T">The type if message to deserialise</typeparam>
    public class DatabaseDataSource<T> : IDataSource where T : IConstrainableMessage
    {
        private string m_sqlConnectionString;
        private SqlParameter[] m_sqlParams;
        private string m_commandText;
        private ILogger m_logger;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="sqlConnectionString">DBMS connection string</param>
        /// <param name="parameters">Command text paramaters</param>
        /// <param name="commandText">The SQL Query</param>
        /// <param name="log">Service logger</param>
        public DatabaseDataSource(string sqlConnectionString, SqlParameter[] parameters, string commandText, ILogger logger)
        {
            m_sqlConnectionString = sqlConnectionString;// DBMS connection string
            m_sqlParams = parameters;// Query paramaters
            m_commandText = commandText;// SELECT Query to get messages
            m_logger = logger;
        }

        /// <summary>
        /// Gets an async enumerator that can be used to pull messages in a peicewise fashion from the DBMS. The SQL connection and transaction are closed
        /// on completion, cancellation and exception.
        /// </summary>
        /// <param name="cancellationToken">A cancellation token that allows the user to cancel the enumeration (could take a while to complete if large data set)</param>
        /// <returns>The async enumerator that can be used to pull the messages</returns>
        public async IAsyncEnumerable<IConstrainableMessage> GetMessagesAsync([EnumeratorCancellation] CancellationToken cancellationToken)
        {
            using SqlConnection connection = new SqlConnection(m_sqlConnectionString);// Create connection to DBMS (uses DB connection pool)

            await connection.OpenAsync(cancellationToken).ConfigureAwait(false);
            using SqlCommand command = new SqlCommand(m_commandText, connection);
            command.Parameters.AddRange(m_sqlParams);

            using var reader = await command.ExecuteReaderAsync()
                .ConfigureAwait(false);

            var parser = reader.GetRowParser<T>();

            while (await reader.ReadAsync(cancellationToken).ConfigureAwait(false))// Data exists
            {
                T element;

                try
                {
                    element = parser(reader);// Uses Dapper to map SQL result to our C# POCO i.e. 'WayflyerMessageV1'
                }
                catch (Exception ex)
                {
                    // Log the error and continue as best we can
                    m_logger.LogError($"Failed to map data for query: {m_commandText} error: {ex}");
                    continue;
                }

                yield return element;
            }
        }
    }
}
