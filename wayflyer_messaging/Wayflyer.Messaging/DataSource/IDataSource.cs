﻿using System.Collections.Generic;
using System.Threading;

namespace Wayflyer.Messaging.DataSource
{
    /// <summary>
    /// This will allow for asynchronous piecewise enumeration of huge data sets providing 'IConstrainableMessage' messages
    /// </summary>
    public interface IDataSource
    {
        /// <summary>
        /// Gets an async enumerator that can be used to pull messages in a piecewise fashion from the data source
        /// </summary>
        /// <param name="cancellationToken">A cancellation token that allows the user to cancel the enumeration (could take a while to complete if large data set)</param>
        /// <returns>The async enumerator that can be used to pull the messages</returns>
        IAsyncEnumerable<IConstrainableMessage> GetMessagesAsync(CancellationToken cancellationToken);
    }
}