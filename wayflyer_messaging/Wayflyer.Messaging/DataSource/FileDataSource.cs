﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using System.Threading;
using Wayflyer.Messaging.Serialiser;

namespace Wayflyer.Messaging.DataSource
{
    /// <summary>
    /// A file data source that can be used to asynchronously read messages from a specified file in a piecewise fashion to avoid memory pressures.
    /// USAGE: FileDataSource<WayflyerMessageV1> fileSource;
    /// </summary>
    /// <typeparam name="T">The type if message to deserialise</typeparam>
    public class FileDataSource<T> : IDataSource where T : IConstrainableMessage
    {
        private const int BufferSizeBytes = 32768;// 32KB
        private string m_filename;
        private IMessageSerialiser<T> m_messageSerialiser;
        private ILogger m_logger;

        /// <summary>
        /// Contructor
        /// </summary>
        /// <param name="filename">The file to read</param>
        /// <param name="serilaiser">Serialiser required to interpret message data</param>
        /// <param name="log">Service logger</param>
        public FileDataSource(string filename, IMessageSerialiser<T> serilaiser, ILogger log)
        {
            m_filename = filename;
            m_messageSerialiser = serilaiser;
            m_logger = log;
        }

        /// <summary>
        /// Gets an async enumerator that can be used to pull messages in a peicewise fashion from the file. The filestream and reader are closed
        /// on completion, cancellation and exception.
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async IAsyncEnumerable<IConstrainableMessage> GetMessagesAsync([EnumeratorCancellation] CancellationToken cancellationToken)
        {
            // This provides fast sequential access
            using var stream = new FileStream(m_filename, FileMode.Open, FileAccess.Read,
                                    FileShare.Read, BufferSizeBytes, FileOptions.Asynchronous | FileOptions.SequentialScan);

            using var reader = new StreamReader(stream);

            while (true)
            {
                var line = await reader.ReadLineAsync().ConfigureAwait(false);
                if (line == null)
                {
                    // We are complete
                    break;
                }

                T message;

                try
                {
                    // Use the serialiser to interpret the message data
                    message = await m_messageSerialiser.Deserialise(line);
                }
                catch (Exception ex)
                {
                    // Log the error and continue as best we can
                    m_logger.LogError($"Failed to deserialise:{m_filename} error: {ex}");
                    continue;
                }

                yield return message;
            }
        }
    }
}
