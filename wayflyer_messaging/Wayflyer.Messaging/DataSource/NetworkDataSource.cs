﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;

namespace Wayflyer.Messaging.DataSource
{
    /// <summary>
    /// A network data source that can receive messages from a remote service (via TCP, WebSocket, gRPC etc.)
    /// NOTE: Not implemented just for illustration.
    /// </summary>
    public class NetworkDataSource<T> : IDataSource where T : IConstrainableMessage
    {
        public async IAsyncEnumerable<IConstrainableMessage> GetMessagesAsync([EnumeratorCancellation] CancellationToken cancellationToken)
        {
            // Read data from the remote connection i.e. socket or grpc using Protobuf serialsier (part of gRPC) and create appropriate message
            while (!cancellationToken.IsCancellationRequested)
            {
                var msg = await ReadRemoteMessage();

                // Socket data exists, deserialise message data and create POCO
                yield return msg;
            }
        }

        private Task<T> ReadRemoteMessage()
        {
            return Task.FromResult(default(T));// MOCK message as example
        }
    }
}
