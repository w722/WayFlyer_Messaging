﻿using System.Threading.Tasks;

namespace Wayflyer.Messaging
{
    /// <summary>
    /// Respresents a message that can be checked against a one or more constraints
    /// </summary>
    public interface IConstrainableMessage
    {
        /// <summary>
        /// Check whether the message satisfies the constraint. Uses the visitor patten to extend message functionality (SOLID)
        /// </summary>
        /// <param name="constraint">The constraint to check</param>
        /// <returns>True if the constraint is satisfied, otherwise false</returns>
        Task<bool> Satisfies(Constraint.IMessageConstraint constraint);
    }
}
