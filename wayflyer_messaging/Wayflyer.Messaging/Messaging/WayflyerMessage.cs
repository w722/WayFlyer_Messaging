﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Wayflyer.Messaging
{
    /// <summary>
    /// Character constraint information. This information is validated in constraint specific ways.
    /// We deliberately don't validate the values here because this is the responsibility of the constraint class implementations (and they can vary).
    /// e.g.'RangeCountConstraint' will allow 0s where as 'PositionConstraint' will not.
    /// Max search string size allowed is short.MaxValue (2^15-1) and Min is 0.
    /// </summary>
    public struct CharacterConstraintInformation
    {
        /// <summary>
        /// A context specific digit <seealso cref="Constraint.IMessageConstraint"/><seealso cref="Constraint.RangeCountConstraint"/><seealso cref="Constraint.PositionConstraint"/>
        /// </summary>
        public short Digit1 { get; private set; }

        /// <summary>
        /// A context specific digit <seealso cref="Constraint.IMessageConstraint"/><seealso cref="Constraint.RangeCountConstraint"/><seealso cref="Constraint.PositionConstraint"/>
        /// </summary>
        public short Digit2 { get; private set; }

        /// <summary>
        /// The character to search for
        /// </summary>
        public char Character { get; private set; }

        /// <summary>
        /// Basic constructor
        /// </summary>
        /// <param name="digit1">First digit</param>
        /// <param name="digit2">Second digit</param>
        /// <param name="matchChar">Character</param>
        public CharacterConstraintInformation(short digit1, short digit2, char matchChar)
        {
            if (digit1 < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(digit1));
            }

            if (digit2 < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(digit2));
            }

            Digit1 = digit1;
            Digit2 = digit2;
            Character = char.ToLower(matchChar);
        }

        public override string ToString()
        {
            // Example: 1-3 a
            return string.Format($"{Digit1}-{Digit2} {Character}");
        }
    }

    /// <summary>
    /// Message V1 structure. POCO (Plain Old CLR Object) to hold character constraint information and search string.
    /// </summary>
    public class WayflyerMessageV1 : IConstrainableMessage
    {
        /// <summary>
        /// Character constraint information
        /// </summary>
        public CharacterConstraintInformation CharInfo {get; private set;}

        /// <summary>
        /// Search string
        /// </summary>
        public string SearchString { get; private set; }

        /// <summary>
        /// Check whether this message satisfies the specified constraint (visit contraints)
        /// </summary>
        /// <param name="constraint">The message constraint to check</param>
        /// <returns></returns>
        async Task<bool> IConstrainableMessage.Satisfies(Constraint.IMessageConstraint constraint)
        {
            return await constraint.IsValid(this);
        }

        /// <summary>
        /// Basic constructor
        /// </summary>
        /// <param name="charInfo">Character constraint information</param>
        /// <param name="searchString">The search string (zero or more characters)</param>
        public WayflyerMessageV1(CharacterConstraintInformation charInfo, string searchString)
        {
            CharInfo = charInfo;
            SearchString = searchString ?? throw new ArgumentException("Search string cannot be null", "SearchString");// We allow empty string

            if (searchString.Length >= short.MaxValue)
            {
                throw new ArgumentOutOfRangeException("Max searchstring size exceedeed", nameof(searchString));
            }

            SearchString = SearchString.ToLower();
        }

        /// <summary>
        /// Stringify the message
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format($"{CharInfo}: {SearchString}");
        }
    }
}
