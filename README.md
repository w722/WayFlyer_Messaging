# Wayflyer Messaging

Core messaging functionality for solving 'Problem2.txt'

.Net Core 5 solution to the Wayflyer problem detailed in 'problem2.txt'. The answers have been inserted as requested into the text file in the root of the project.

For unit testing I have used the following additional NuGet packages: xUnit, NSubstitute and FluentAssertions and Nerdbank.GitVersioning (deploy versioning)

In solving this problem I've tried to showcase some OO design skills as well as CI/CD and Docker ones.

# Design

Uses SOLID design principles to achieve a clear seperation of concerns between message structure (and versioning), message serialisation formats, data input sources and messaging constraints. I’ve tried to envisage some common scenarios, message format or structure change/evolution, serialiser change (JSON, XML etc.), input data sources (file, database, network), input data size growth/expansion,
specifically dealing with huge data sets.

## Restore and Build

The '.gitlab-ci.yml' in the root of the project details the CI stages/commands used restore and build the solution.

The solution will require https://dotnet.microsoft.com/en-us/download/dotnet/5.0 (.NET 5 runtime to run natively) or can be run in Docker ([##Running locally with Docker]) 

Run the following from the root folder:

'dotnet restore wayflyer_messaging\/WayflyerMessaging.sln' (*Restore*)

'dotnet msbuild wayflyer_messaging\/WayflyerMessaging.sln \/p:Configuration=Release' (*Build*)

'dotnet test wayflyer_messaging\/WayflyerMessaging.sln \/p:Configuration=Release' (*Run Tests*)

# Repo and CI/CD and Docker

The Repo (git) and CI/CD exist on GitLab

Repository: https://gitlab.com/w722/WayFlyer_Messaging

Clone with HTTPS (https://gitlab.com/w722/WayFlyer_Messaging.git)

CI/CD Pipelines: https://gitlab.com/w722/WayFlyer_Messaging/-/pipelines

I've setup up a CI pipeline with build, test and deploy stages. The core package is built and tested on the CI system and using 'coverlet', a code coverage report is generated and output. The pipeline setup is in the '.gitlab-ci.yml' file.

If you go to the pipeline and click on the test stage, you can view the output the unit tests and the coverage report.

A versioned NuGet package is then created and deployed.

## Running locally with Docker

In './WayflyerMessaging/wayflyer_messaging' a 'Dockerfile' exists that runs the 'Wayflyer.Messaging.Solution' console application and also generates and outputs the code coverage report from the unit test project.

https://docs.docker.com/get-docker/ (To get Docker)

From './WayflyerMessaging/wayflyer_messaging' folder where the 'Dockerfile' resides, run 'docker build -t wayflyer-app .' after this completes running 'docker run wayflyer-app:latest' will run the 'Solution' exe and the unit test project and 
output the test coverage report.

# Project Structure

The top-level Solution 'WayflyerMessaging' contains 3 sub-projects:

**Wayflyer.Messaging** - The core package which includes:

1. **Constraints**: (Messaging constrainsts that can be applied to messages, there are currently two types (RangeCount and Position). These are used to indicate whether a message is valid or not and are injected in (visited).
2. **Data Source**: (Input data source abstraction (IDataSource) including concrete implmentations (File, Database, Network), only 'File' is completely useable, althougth the database one is implementated (uses Dapper ORM).
3. **Messaging**: Messaging POCOs(Plain Old CLR Objects)
4. **Serialiser**: Messaging (de)serialisation functionality, string to POCO and visa versa. The default serialiser 'FormatStringMessageSerialiser' uses the current prescribed problem format, but we may well want to change to using something else like JSON or XML in the future.
   
**Wayflyer.Messaging.Solution** - Console application that specifically reads in 'Input2.txt' data and solves the questions posed in problem2.txt and logs out the solutions (Problem2Solution.cs). This uses the core functionality 
in 'Wayflyer.Messaging'.

**Wayflyer.Messaging.UnitTest** - The unit test project. Contains unit tests cases for each of the core sections discussed above i.e. Constraints, Data Source, Messaging and Serialiser.

## Unit Test Omissions

Some functionality has been added for illustrative purposes only and as such has no test coverage (omissions include: Wayflyer.Messaging.DataSource.DatabaseDataSource<T>, Wayflyer.Messaging.DataSource.NetworkDataSource<T> and 
Wayflyer.Messaging.Serialiser.JSONStringMessageSerialiser). The good thing is the coverage report cleary highlights this :see_no_evil:
